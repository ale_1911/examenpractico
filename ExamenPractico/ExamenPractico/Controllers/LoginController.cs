﻿using ExamenPractico.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ExamenPractico.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Home/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Autorizado(ExamenPractico.Models.Usuario userModel)
        {
           
            using (ExamenPracticoDBEntities1 db = new ExamenPracticoDBEntities1())
            {
                var usuarioDetalles = db.Usuario.Where(x => x.usuario1 == userModel.usuario1 &&
                    x.contraseña == userModel.contraseña).FirstOrDefault();

                if (usuarioDetalles == null)
                {
                    userModel.mensajeError = "Usuario o contraseña incorrecto";
                    return View("Index",userModel);
                }
                else {

                    var rolDetalle = usuarioDetalles.Rol.nombre;

                    Session["idUsuario"] = usuarioDetalles.idUsuario;
                    Session["nombre"] = usuarioDetalles.nombre;
                    Session["usuario"] = usuarioDetalles.usuario1;
                    Session["nombreRol"] = rolDetalle;
                    return RedirectToAction("Index", "Home");
                    
                    
                }
            }
            
        }

        public ActionResult LogOut()
        {
            Session.Abandon();
            return RedirectToAction("Index","Login");
        }
    
    }
}
